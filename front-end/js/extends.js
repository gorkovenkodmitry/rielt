var onReady = {
    init: function() {
        onReady.config = {
            commandBlock: $('.command-blocks'),
            contactsBlock: $('.contacts-block'),
            startAnimate: false,
            timeAnimation: 500,
            commandCount: 24,
            swipeMove: false,
            wow: null,
            commandMoveCoords: null
        };

        onReady.setup();

    },

    setup: function() {
        onReady.config.commandBlock.on('touchstart', onReady.commandMoveStart);
        onReady.config.commandBlock.on('touchmove', onReady.commandMove);
        onReady.config.commandBlock.on('touchend', onReady.commandMoveEnd);

        onReady.config.commandBlock.on('click', '.ws_prev', onReady.commandLeft);
        onReady.config.commandBlock.on('click', '.ws_next', onReady.commandRight);

        $(document).on('click', '.guarantees-btn', onReady.guaranteesBtnClick);

        $(document).on('click', '.openContactsForm', onReady.openContactsForm);
        $(document).on('click', '.sendContactsForm', onReady.sendContactsForm);
        $(document).on('click', '.openContactsBlock', onReady.openContactsBlock);
        $(document).on('click', '.agencies_header_search_menu a', onReady.scrollMenuClick);

        $('.select2').select2({
            minimumResultsForSearch: Infinity,
            placeholder: 'Кто вы'
        });

        $(document).on('focus', '.maskcellphone', function() {
            $(this).mask('+9 999 999 9999');
        });

        onReady.commandSetup();
        onReady.setupWow();
    },

    setupWow: function() {
         $('.work-block').addClass('wow').addClass('fadeInUp');
        if ($('body').width() < 995) {
            $('.work-block').attr('data-wow-duration', '1s');
        } else {
            $('.work-block.left').attr('data-wow-duration', '1s');
            $('.work-block.right').attr('data-wow-duration', '1s').attr('data-wow-delay', '0.25s');
        }
        $('.wow').each(function() {
            if (!$(this).attr('data-wow-duration')) {
                $(this).attr('data-wow-duration', '2s');
            }
        });

        $('.advantages-block').each(function(ind) {
            $(this).attr('data-wow-duration', '1s');
            if ($('body').width() < 995) {
                $(this).attr('data-wow-delay', (ind%2*0.25) + 's');
            } else {
                $(this).attr('data-wow-delay', (ind%3*0.25) + 's');
            }
        });

        onReady.config.wow = new WOW();
        onReady.config.wow.init();
    },

    scrollMenuClick: function(e) {
        e.preventDefault();
        var btn = $(this);
        btn.parent().parent().find('li').removeClass('active');
        btn.parent().addClass('active');
        $('html, body').animate({
            scrollTop: $(btn.attr('data-scroll')).offset().top - 150
        }, onReady.config.timeAnimation);
    },

    openContactsForm: function(e) {
        e.preventDefault();
        onReady.config.contactsBlock.find('.before-form').addClass('hide');
        onReady.config.contactsBlock.find('.contacts-form').removeClass('hide');
        onReady.config.contactsBlock.find('.after-form').addClass('hide');
    },

    sendContactsForm: function(e) {
        e.preventDefault();
        onReady.config.contactsBlock.find('.before-form').addClass('hide');
        onReady.config.contactsBlock.find('.contacts-form').addClass('hide');
        onReady.config.contactsBlock.find('.after-form').removeClass('hide');
    },

    openContactsBlock: function(e) {
        e.preventDefault();
        onReady.config.contactsBlock.find('.before-form').removeClass('hide');
        onReady.config.contactsBlock.find('.contacts-form').addClass('hide');
        onReady.config.contactsBlock.find('.after-form').addClass('hide');
    },

    commandRight: function(e) {
        e.preventDefault();
        if (!onReady.config.startAnimate) {
            onReady.config.startAnimate = true;
            var curr = onReady.config.commandBlock;
            var wrap = curr.find('.wrap');
            // if (wrap.width() > curr.width()) {
                var offset = parseFloat(wrap.css('margin-left'));
                var max_offset_left = - wrap.width()*onReady.config.commandCount/2;
                var max_offset_right = wrap.width()*onReady.config.commandCount/2;
                offset -= 178*2;
                if (offset < max_offset_left) {
                    offset = max_offset_left;
                } else if (offset > max_offset_right) {
                    offset = max_offset_right;
                }
                wrap.animate({
                    'margin-left': offset+'px'
                }, onReady.config.timeAnimation, done=function() {
                    onReady.config.startAnimate = false;
                });
            // }
        }
        return true;
    },

    commandLeft: function(e) {
        e.preventDefault();
        if (!onReady.config.startAnimate) {
            onReady.config.startAnimate = true;
            var curr = onReady.config.commandBlock;
            var wrap = curr.find('.wrap');
            // if (wrap.width() > curr.width()) {
                var offset = parseFloat(wrap.css('margin-left'));
                var max_offset_left = - wrap.width()*onReady.config.commandCount/2;
                var max_offset_right = wrap.width()*onReady.config.commandCount/2;
                offset += 178*2;
                if (offset < max_offset_left) {
                    offset = max_offset_left;
                } else if (offset > max_offset_right) {
                    offset = max_offset_right;
                }
                wrap.animate({
                    'margin-left': offset+'px'
                }, onReady.config.timeAnimation, done=function() {
                    onReady.config.startAnimate = false;
                });
            // }
        }
        return true;
    },

    commandMove: function(e) {
        var coords = e.originalEvent.targetTouches[0];
        if (onReady.config.commandMoveCoords) {
            var curr = onReady.config.commandBlock;
            var wrap = curr.find('.wrap');
            if (wrap.width() > curr.width()) {
                var percent = (coords.pageX-onReady.config.commandMoveCoords.pageX) / curr.width()*4;
                var offset = parseFloat(wrap.css('margin-left'));
                var max_offset_left = - wrap.width()*onReady.config.commandCount/2;
                var max_offset_right = wrap.width()*onReady.config.commandCount/2;
                offset += (wrap.width()-curr.width())*percent;
                if (offset < max_offset_left) {
                    offset = max_offset_left;
                } else if (offset > max_offset_right) {
                    offset = max_offset_right;
                }
                wrap.css({
                    'margin-left': offset+'px'
                });
            }
        }
        onReady.config.commandMoveCoords = coords;
        return true;
    },

    commandMoveStart: function(e) {
        onReady.config.commandMoveCoords = e.originalEvent.targetTouches[0];
        return true;
    },

    commandMoveEnd: function(e) {
        onReady.config.commandMoveCoords = null;
        return true;
    },

    commandSetup: function() {
        var curr = onReady.config.commandBlock;
        var wrap = curr.find('.wrap');
        var multi = wrap.find('.multi').clone();
        wrap.find('.multi').remove();
        for (var i=0; i<(onReady.config.commandCount+1); i++) {
            multi.css({
                position: 'absolute',
                left: (i-onReady.config.commandCount/12)*wrap.width()+'px',
                width: wrap.width()+'px'
            });
            wrap.append(multi);
            multi = multi.clone();
        }
    },

    guaranteesBtnClick: function(e) {
        e.preventDefault();
        var btn = $(this);
        if (btn.hasClass('active')) return false;
        if (onReady.config.startAnimate) return false;
        onReady.config.startAnimate = true;
        $('.guarantees-btn').removeClass('active');
        btn.addClass('active');
        var curr_block = $('.guarantees-block.active');
        var next_block = $('.guarantees-block[data-block="'+btn.attr('data-block')+'"]');
        var wrap = $('.guarantees-blocks .wrap');
        curr_block.css({
            'position': 'absolute',
            'top': 0
        });
        var offset = (wrap.width() - curr_block.width()) / 2.0;
        curr_block.css({
            'left': offset+'px'
        });
        curr_block.find('.text').animate({
            'opacity': 0
        }, onReady.config.timeAnimation/2, done=function() {
            curr_block.find('.img').animate({
                'opacity': 0
            }, onReady.config.timeAnimation, done=function() {
                curr_block.addClass('hide').removeClass('active');
                curr_block.css({left: '0'});
            });
        });

        next_block.removeClass('hide').addClass('active').css({
            'position': 'relative',
        });
        next_block.find('.img').css({
            'opacity': 0
        });
        next_block.find('.text').css({
            'opacity': 0
        });
        next_block.find('.img').animate({
            'opacity': 1
        }, onReady.config.timeAnimation, done=function() {
            setTimeout(function() {
                next_block.find('.text').animate({
                    'opacity': 1
                }, onReady.config.timeAnimation/2, done=function() {
                    onReady.config.startAnimate = false;
                });
            }, onReady.config.timeAnimation/2);
        });
        
    }
};

$(document).ready(onReady.init);
$(window).resize(onReady.setup);
