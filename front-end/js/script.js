/*======================================================================

 =            Обработка скролла и параллакс-эффектов в шапке            =

 ======================================================================*/
// От значения этого индикатора зависит, какой вид параллакса будет использоваться

var PARALLAX_TYPE;
var agencies_myMap = {};

// Функция установки значения PARALLAX_TYPE в зависимости от URL
function setParallax(argument) {

  lineId = document.getElementById("steps_line");
  animateBlockId = document.getElementById("animate");

  if (lineId && lineId.classList.contains("m-agencies")) {
    lineId.classList.remove("m-agencies");
    animateBlockId.classList.remove("m-agencies");
  }

  switch (window.location.pathname) {

    case "/all-agencies":
      PARALLAX_TYPE = "middle";
      lineId.classList.add("m-agencies");
      animateBlockId.classList.add("m-agencies");
      break;
    case "/crm-for-agency":
      PARALLAX_TYPE = "small";
      break;
    case "/login":
      PARALLAX_TYPE = "small";
      break;
    case "/howitworks":
      PARALLAX_TYPE = "howitworks";
      break;
    case "/registration":
      PARALLAX_TYPE = "small";
      break;
    default:
      PARALLAX_TYPE = "full";
  }

  return PARALLAX_TYPE;
}

// Функция обработки параллакс-эффекта
function goParallax(type) {

  var pageY = window.pageYOffset || document.documentElement.scrollTop,
    scrollHeader = $('.r_header'),
    scrollBlurLine = $('.r_line'),
    stepsBlock = $('.r_steps__wrapper'),
    pageBlock = $('.main'),
    bigContent = $('.content.m-notice, .content.m-howitworks'),
    smallContent = $('.content_1'),
    scrollHeaderHeigh = scrollHeader.height(),
    scrollBlurLineHeight = scrollBlurLine.height(),
    cartBlock = $('#agencies_map'),
    mapInfoBlock = $('.map__infoblock'),
    searchBlock = $('.agencies_header_search_line__wrapper'),
    searchBlockHeight = searchBlock.height(),
    searchBg = $('.agencies_header_search_helper'),
    buttonsBlock = $('.agencies_buttons');

  cartBlock.css({
    'width': $('.agencies_blocks').width() - 770
  })

  // Параллакс на страницах, где только название раздела и нет особых эффектов кроме закрепления шапки
  if (type === "small") {
    scrollHeader.css("background-position", "0 -" + (pageY / 10))
  }

  if (type === "full" || type === "middle" || type === "howitworks") {

    if (pageY > (scrollHeaderHeigh - searchBlockHeight) && pageY <= searchBlockHeight) {
      scrollBlurLine.removeClass('js-fixed').addClass("bottom_absolute");
      searchBlock.removeClass('js-fixed');
      searchBg.removeClass('js-fixed');
      buttonsBlock.removeClass('js-fixed');

      bigContent.removeClass('with_fixed_header');

      if (stepsBlock) {
        stepsBlock.removeClass('js-fixed');
      }

      if (cartBlock) {
        cartBlock.removeClass('js-fixed');
        cartBlock.css({
          'top': 0,
        });

        if (mapInfoBlock.hasClass('js-hidden'))
          mapInfoBlock.removeClass('js-hidden');

      };


    } else if (pageY > scrollHeaderHeigh - 37) {

      searchBg.addClass('js-fixed');
      searchBlock.addClass('js-fixed');
      $(".animatediv").css({"top": -(pageY / 10), "position": "relative"});

      bigContent.addClass('with_fixed_header');

      if (stepsBlock) {
        stepsBlock.addClass('js-fixed');
      }

      if (pageY > scrollHeaderHeigh - 23) {
        buttonsBlock.addClass('js-fixed');
        $('.vip_agencies').css('margin-top', 58);
      } else {
        buttonsBlock.removeClass('js-fixed');
        $('.vip_agencies').css('margin-top', 0);
      }

      if (cartBlock) {

        cartBlock.addClass('js-fixed');
        cartBlock.css({
          'top': scrollBlurLineHeight + 64,
          'left': -3,
          'width': $('.agencies_blocks').width() - 778,
        })

        $('.vip_agencies__add_agency_fixed').css('top', 145);

        if (pageY >= (pageBlock.height() - $(window).height()) - 20) {
          if (!mapInfoBlock.hasClass('js-static')) {
            mapInfoBlock.addClass('js-hidden');
          }
          ;
        } else {
          if (mapInfoBlock.hasClass('js-hidden'))
            mapInfoBlock.removeClass('js-hidden');
        }

      };


    } else {
      $(".animatediv").css({"top": -(pageY / 10), "position": "relative"});
      scrollBlurLine.addClass('js-fixed').removeClass("bottom_absolute");
      searchBlock.removeClass('js-fixed');
      searchBg.removeClass('js-fixed');
      buttonsBlock.removeClass('js-fixed');
      scrollHeader.css("background-position", "0 -" + pageY / 3)
      $('.vip_agencies').css('margin-top', 0);

      bigContent.removeClass('with_fixed_header');

      if (stepsBlock) {
        stepsBlock.removeClass('js-fixed');
      }

      if (cartBlock) {
        cartBlock.removeClass('js-fixed');
        cartBlock.css({
          'top': 0,
          'left': 0,
          'width': $('.agencies_blocks').width() - 779,
        });
      }
      ;

      $('.vip_agencies__add_agency_fixed').css('top', 103);

      if (!mapInfoBlock.hasClass('js-hidden'))
        mapInfoBlock.removeClass('js-hidden');

      if (agencies_myMap) {
        if (agencies_myMap.options) {
          agencies_myMap.container.fitToViewport();
        }
      }
    }
  }
  // Если переменная PARALLAX_TYPE не указана
}

// Закрепляем футер к низу на странице добавления объявления
function moveFooter () {
  var footer = $('.footer.m-notice');

  if ($(window).height() > 815 && $('.content.m-notice').height() < 600) {
    footer.addClass('js-fixed');
  } else {
    footer.removeClass('js-fixed');
  }

  console.log($(window).height());
}

$(function () {
  // Запускаем функцию при загрузке страницы
  setParallax();
  moveFooter();
});

$('window').on('resize', function(){
  moveFooter();
})

// Запускаем параллакс при скролле страницы
window.onscroll = function () {
  goParallax(PARALLAX_TYPE);
}

$(function () {
  goParallax(PARALLAX_TYPE);
});

/*-----  End of Обработка скролла и параллакс-эффектов в шапке  ------*/


/*=============================================================================
 =            Изменение скролла на разных экранах для скрытия шапки            =
 =============================================================================*/

// Вспомогательная функция определения высоты браузера
function getWindowHeight() {
  if (document.documentElement && document.documentElement.clientHeight) {
    return document.documentElement.clientHeight;
  } else if (document.body) {
    return document.body.clientHeight;
  }
}

function setHeightPage() {
  windowHeight = getWindowHeight();
  if (windowHeight < 768 && windowHeight > 550 && PARALLAX_TYPE == "full") {
    y = windowHeight - 312;
    $(".content#an").css("margin-bottom", y);
  } else {
    $(".content#an").css("margin-bottom", 0);
  }
}

/*-----  End of Изменение скролла на разных экранах для скрытия шапки  ------*/

// });

// Появление правого меню в шапке
$(function () {
  $('.topmenu').click(function () {
    $(this).toggleClass('active');
  });

  // При скролле скрывать меню
  $(window).scroll(function (event) {
    if ($(".topmenu").hasClass('active')) {
      $(".topmenu").removeClass('active');
    }
  });

  // При нажатии на любом другом месте скрывать меню
  $(document).click(function (event) {
    if ($(event.target).closest(".topmenu").length)
      return;
    $(".topmenu").removeClass('active');
    event.stopPropagation();
  });
});

function applySelect() {
  $(".chosen-select").chosen({disable_search_threshold: 10});
  $(".chosen-search").hide();
  $(".chosen-single").addClass("drop-width");
  $(".chosen-drop").addClass("drop-ul-with");
  $(".chosen-container-single").css({"width": "100%"});
  $(".chosen-container-single .chosen-single span").addClass("ttop");
  $(".chosen-container .chosen-results li").addClass("hhov");
}

$(function () {
  $('.content').on('change', '.select_main', function () {
    $(this).find('.ttop').addClass("active");
  });
});
//ввод чисел в формате 17 458.456
function splitFloat(delimiter, str) {
  if (str == ',') return '';
  str = str.replace(/[^\d,|\.]/g, "");
  str = str.replace(/(,|\.)(.*?)\1/ig, '$1$2');
  //str = str.replace(/.(.*?)\1/ig, '$1$2');
  str = str.replace(/(\d+)((,|\.)\d+)?/g,
    function (c, b, a) {
      return b.replace(/(\d)(?=(\d{3})+$)/g, '$1' + delimiter) + (a ? a : '')
    }
  );
  return str;
}
//ввод чисел в формате 17 458
function splitNum(delimiter, str) {
  if (str == ',') return '';
  str = str.replace(/[^\d]/g, "");
  str = str.replace(/(\d+)(\d+)?/g,
    function (c, b, a) {
      return b.replace(/(\d)(?=(\d{3})+$)/g, '$1' + delimiter) + (a ? a : '')
    }
  );
  return str;
}

$('.num').each(function () {
  this.value = splitNum(' ', this.value);
});

$('.float').each(function () {
  this.value = splitFloat(' ', this.value);
});

$(document).on('keyup', '#add_form .float', function () {
  this.value = splitFloat(' ', this.value);
  $(this).trigger('change');
});

$(document).on('keyup', '#add_form .num', function () {
  this.value = splitNum(' ', this.value);
  $(this).trigger('change');
});

$(function () {
  //************************* Форма добавления обявления ***************************************


  //Закрытие уведомления

  $(document).on('click', '.close_err', function () {
    $(this).parent().parent().fadeOut(400);
  });
  $('.mob').focus(function () {
    $('.repeat_phone').show();
    //$('.payment_type_block').show();
    //$('#check_your_phone').show();
  });
  $('.mob').change(function () {
    $('.repeat_phone').show();
    if ($(this).val().length > 3) {
      $('#ins_your_phone').html($('.mob').val());
      $('#check_your_phone').show();
    }
  }).keypress(function () {
    $('#check_your_phone').hide();
  });
  $('#ddd').click(function () {
    $('.anv').animate({top: '-307'}, 1000, function () {
    });

    $('.fill_data_buttons ').animate({top: '-207'}, 1000, function () {
    });

    $(".head_img_back").animate({height: "175"}, 1000);
    $(".content").css({'top': 176});
    $(".head_img_back").css({'position': 'fixed', 'top': 0});
    return false;
  });
  // jQuery(document).ready(function () {
  //   $objWindow = $(window);
  //   $('div[data-type="background"]').each(function () {
  //     var $bgObj = $(this);
  //     $(window).scroll(function () {
  //       var yPos = -($objWindow.scrollTop() / $bgObj.data('speed'));
  //       var paral = '50% ' + yPos + 'px';
  //       $bgObj.css({backgroundPosition: paral});
  //     });
  //   });
  // });  //************************* Форма регистрации ***************************************

  /*var cities = ["Владивосток", "Волгоград", "Москва", "Иркутск", "Нижний Новгород", "Самара", "Челябинск", "Чита", "Уфа", "Хабаровск"];
   $('#city').autocomplete({
   source: cities,
   });  */

  // После ввода города появляется поле регион

  $('#city').blur(function () {

    var citylength = $('#city').val().length;
    if (citylength == 0) {
      $('#region').hide();
    } else if (citylength >= 1) {
      $('#region').show();
    }

  });
  $('#agent').focusin(function () {
    if (!this.value)
      this.value = 'АН ';
  }).focusout(function () {
    if (this.value == 'АН ')
      this.value = '';
  });
  // Ввод только цифр
  $("#korpus,#floor,#floornomer,#area").keydown(function (event) {

    // Разрешаем: backspace, delete, tab и escape
    if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 190 || event.keyCode == 188 ||
        // Разрешаем: Ctrl+A
      (event.keyCode == 65 && event.ctrlKey === true) ||

        // Разрешаем: home, end, влево, вправо

      (event.keyCode >= 35 && event.keyCode <= 39)) {
      // Ничего не делаем
    }
    else {
      // Обеждаемся, что это цифра, и останавливаем событие keypress
      if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {

        event.preventDefault();
      }
    }

  });
  //Маска для поля телефон
  jQuery(function ($) {

    //$('#phone').mask('+79999999999');
    //$('#phone2').mask('+79999999999');
    $('.mob').mask('+79999999999');
  });
  // Проверка пароля на надежность
  /* отключена из-за того, что сами поля убрали
   $('#passw').keyup(function (e) {
   var pswd = $(this).val();
   var strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
   var mediumRegex = new RegExp("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
   var enoughRegex = new RegExp("(?=.{6,}).*", "g");
   if (false == enoughRegex.test($(this).val())) {
   $('#not_reliable').html('Не надежный');
   if (pswd == 0) {
   $('#not_reliable').hide();
   }
   } else if (strongRegex.test($(this).val())) {
   $('#reliable').className = 'ok';
   $('#reliable').html('Надежный');
   $('#not_reliable').hide();
   $('#reliable').show();
   } else if (mediumRegex.test($(this).val())) {
   $('#not_reliable').className = 'alert';
   $('#not_reliable').html('Не надежный');
   $('#not_reliable').show();
   $('#reliable').hide();
   }
   return true;
   });
   */

  // Проверка на совпадения паролей
  $('#passw1').keyup(function () {
    if (document.getElementById('passw').value == document.getElementById('passw1').value) {
      $('#not_matches').hide();
      $('#matches').show();
    } else {
      $('#matches').hide();
      $('#not_matches').show();
    }
  });

  // Переключение классов для чекбоксов
  $(document).on('click', '.checkbox', function () {
    var $this = $(this);
    $this.toggleClass('active');
    $this.find('input').val($this.hasClass('active') ? 1 : 0).trigger('change');
  });

  $(document).on('click', '.checking', function () {
    var checkbox = $(this).prev();
    checkbox.toggleClass('active');
    checkbox.find('input').val(checkbox.hasClass('active') ? 1 : 0).trigger('change');
  });

  // Проверка полей формы регистрации

  //выделяем текст инпута автокомплита
  $('#city').on('focus', function () {
    $(this).select();
  })

  //************************* Форма Авторизации ***************************************

  $('#remind').click(function () {
    $('.forgot_pass_block').fadeIn('slow');
  });

  // $('input,textarea').focus(function () {
  //   $(this).data('placeholder', $(this).attr('placeholder'));
  //   $(this).attr('placeholder', '');
  // });

  // $('input,textarea').blur(function () {
  //   $(this).attr('placeholder', $(this).data('placeholder'));
  // });

  // Удаление фото с эффектом
  $(document).on('click', '.load_block_close', function () {
    //$('.load_block_close').click(function () {
    $(this).parent().parent().fadeOut('slow', function () {
      $(this).remove();
      var amountImg = $('.list_photo_div').children().length;
      if (amountImg == 0) {
        $('.list_photo_div').remove();
      }
    });
    return false;
  });

  //************************* Отображение списка страниц - ajax ***************************************

  var toMain = true;

  // нажатие на кнопку список агентств
  var loadingAgenciesFlag = false;
  $('.js_open_agencies_right').click(function (e) {
    return;
    //e.preventDefault();
    //console.log("rgerg");
    $('.js_open_agencies').trigger("click");
    return false;
  });

  $('.js_open_agencies').click(function (e) {
    return;
    $(this).addClass("wait");
    e.preventDefault();

    if (loadingAgenciesFlag) return false;
    loadingAgenciesFlag = true;
    window.history.pushState({"pageTitle": "РосРиэлт | Каталог всех агентств недвижимости города " + jsParams['currentCityName']}, "", "/all-agencies");
    document.title = "РосРиэлт | Каталог всех агентств недвижимости города " + jsParams['currentCityName'];

    toMain = false;
    $.ajax({
      type: 'get',
      url: jsParams['agenciesUrlList'],
      data: {cityId: jsParams['currentCityId']},
      dataType: 'json',
      success: function (data) {
        $('#add_form').css('display', 'none');
        // $('.r_steps').addClass("hidden");
        $('#agenciesListWrapper').remove();
        $('#page_content').append(data.data);
        jsParams['agenciesPage'] = data.agenciesPage;
        jsParams['agenciesPagesCount'] = data.agenciesPagesCount;
        loadingAgenciesFlag = false;

        $('.js_send_for_all').removeClass("hidden");
        $('.js_open_agencies').addClass("hidden").removeClass("wait");
        // Изменяем переменную для параллакса
        setParallax();
      },
      error: function () {
        loadingAgenciesFlag = false;
      }
    });

    return false;

  });

  // нажатие на кнопку отправить
  $('.js_send_for_all').click(function (e) {
    return;
    var self = $(this);
    if (toMain) return true;
    e.preventDefault();
    window.history.pushState({"pageTitle": "РосРиэлт | Отправить объявление во все агентства недвижимости города " + jsParams['currentCityName']}, "", "/");
    document.title = "РосРиэлт | Отправить объявление во все агентства недвижимости города " + jsParams['currentCityName'];

    $('body').animate({scrollTop: 0}, 700, function () {
      // Изменяем переменную для параллакса
      setParallax();

      $('#agenciesListWrapper').remove();
      $('#add_form').css('display', 'block');
      $('#headerButtonsAll .js_send_for_all').addClass("hidden");
      $('.js_open_agencies').removeClass("hidden");
      // $('.r_steps').removeClass("hidden");
    });

    return false;
  });
});

// прокрутка списка агентств
var scrollerSet = false;
function loopAgenciesList() {
  // скрываем стандартный навигатор
  $('.pager').hide();
  // запоминаем текущую страницу и их максимальное количество
  var page = jsParams['agenciesPage'] ? jsParams['agenciesPage'] : 1;
  var loadingFlag = false;

  function scroller() {
    var pageCount = jsParams['agenciesPagesCount'] ? jsParams['agenciesPagesCount'] : 0;
    // защита от повторных нажатий
    if (!loadingFlag) {
      var pageStop = $(document).height() - $(window).height() - 100;

      if ($(window).scrollTop() > pageStop) {
        //console.log($(window).scrollTop(), $(document).height() - $(window).height());
        // выставляем блокировку
        loadingFlag = true;
        // отображаем анимацию загрузки
        $('#loading').show();

        $('.map__infoblock').removeClass('js-hidden');
        // Скрываем футер
        $('.footer').addClass('js-hidden');
        $.ajax({
          type: 'get',
          url: jsParams['agenciesUrlList'],
          data: {
            cityId: jsParams['currentCityId'],
            page: page + 1 // передаём номер нужной страницы методом POST
          },
          beforeSend: function () {
            $('.map__infoblock').addClass('js-static');
          },
          success: function (data) {
            // увеличиваем номер текущей страницы и снимаем блокировку
            page++;
            loadingFlag = false;
            // прячем анимацию загрузки
            $('#loading').hide();
            // вставляем полученные записи после имеющихся в наш блок
            $('#agencies-list').find('.items').append($(data).find('.items').html());

            $('.map__infoblock').removeClass('js-static');

            $('.footer').removeClass('js-hidden');

            StoryBox.Init($('body'), {});

            // если достигли максимальной страницы, то прячем кнопку
            if (page >= pageCount)
              loadingFlag = true;

          }
        });
      }
    }
    return false;
  }

  $(window).scroll(scroller);
}

// Главная страница - форма входа


$(function () {

  var plus = $('.js-city_add__button'),
    descrBlock = $('.change_region__description'),
    formBlock = $('.change_region'),
    animateBlock = $('.change_region__wrapper'),
    inputBlock = $('.change_region__input'),
    results = $('.change_region__results'),
    subtext = $('.change_region__wrap_input__text_bottom'),
    hiddenButton = $('.change_region__wrap_input__hidden_button'),
    cityHiddenButton = $('.city_input__hidden_button'),
    streetChekBox = $('#Notice_notStreet'),
    streetAnimateBlock = $('.change_street__wrapper'),
    actionBlock = $('.js-action-type');

  // Появление блока выбора региона и описания при нажатии на +
  plus.click(function () {
    animateBlock.toggleClass('js-hidden');

    if (plus.hasClass) {
      $('#chose_many_regions').tagit("removeAll");
    }

    plus.toggleClass("active");

    // Убрать выделение с поля выбора действия
    if (!animateBlock.hasClass('js-hidden')) {
      actionBlock.removeClass('error');
    } else {
      actionBlock.addClass('error');
    }


    // Всегда делать поле активным при нажатии на плюс
    $('.tagit.ui-widget').addClass('error');

    $('.change_region__wrap_input .tagit-new .ui-autocomplete-input').trigger('focus');

    return false;
  });


  // Появление блока карты при выборе чекбокса "Нет улицы"
  streetChekBox.change(function () {
    if (streetChekBox.val() == 1) {
      streetAnimateBlock.removeClass('js-hidden');
      myMap.container.fitToViewport();
    } else {
      streetAnimateBlock.addClass('js-hidden');
    }

  });


  // Нажатие на стрелку вниз в поле выбора региона
  hiddenButton.click(function () {

    var self = $(this),
      inputNew = self.siblings('.tagit').find('.tagit-new').find('.ui-autocomplete-input'),
      list = self.parent().siblings('.ui-autocomplete');

    if (list.css("display") !== "none") {
      list.css("display", "none");
    } else {
      inputNew.attr('placeholder', 'Введите 3 первые буквы Вашего города')
        .trigger('focus')
        .autocomplete("search", "");
      console.dir(list);
    }

  });

  // Нажатие на стрелку вниз в поле текущего города
  cityHiddenButton.click(function () {

    input = $(this).siblings('.ui-autocomplete-input');

    input.trigger('focus')
      .attr('placeholder', 'Введите 3 первые буквы Вашего города')
      .val("")
      // .trigger('focus')
      // .autocomplete( "search", "" )
    ;
  });

  inputBlock.keypress(function (event) {
    subtext.addClass('hidden')
  });

  $('.tagit ui-autocomplete-input').focusin(function (event) {
    $(this).closest(".tagit").removeClass("error");
  });

  $('.tagit ui-autocomplete-input').focusout(function (event) {
    if ($(this).closest(".tagit").siblings('.change_region__input').val == "") {
      $(this).closest(".tagit").addClass("error");
    }

  });

});

$(function () {

  $(window).on('keydown', function (event) {
    var keyLabels;
    if (event.keyCode == 40) {
      if (keyLabels = document.body.getElementsByClassName("ul-state-focus")) {
        // keyLabels.parent(".ui-menu-item").addClass("active");
        console.dir(keyLabels);
      }
    }
    ;
  });

  $('js-action-type').attr('tabindex', '1');


  // Страница списка агентств - Нажали на кнопку Спросить

  $('body').on('click', '.js-vip-agencies-show-form', function (event) {
    var parentBlock = $(this).closest('.vip_agencies__item'),
      infoBlock = parentBlock.find('.vip_agencies__info'),
      formBlock = parentBlock.find('.vip_agencies__form');

    infoBlock.animate({opacity: 0}, 200, function () {
      infoBlock.css({'z-index': -1, 'display': 'none'});
      formBlock.css({'z-index': 1, 'display': 'block'}).animate({opacity: 1}, 200);
    })

    $('.vip_agencies__form__first_close').click(function () {
      var self = $(this),
        parentBlock = $(this).closest('.vip_agencies__item'),
        infoBlock = parentBlock.find('.vip_agencies__info'),
        formBlock = parentBlock.find('.vip_agencies__form');

      formBlock.animate({opacity: 0}, 200, function () {
        formBlock.css({'z-index': -1, 'display': 'none'});
        infoBlock.css({'z-index': 1, 'display': 'block'}).animate({opacity: 1}, 200);
      })

      return false;
    })

    return false;

  });

  $('body').on('click', '.js-vip-agencies-send', function (event) {
    var self = $(this),
      form = self.closest('.vip_agencies__form'),
      messageBlock = form.find('.vip_agencies_inputs'),
      emailInput = form.find('.vip_agencies__form__email'),
      emailValue = emailInput.val();
    okBlock = $('<div class="vip_agencies__form__ok js-hidden">Ваше сообщение успешно отправлено в ' + $(this).closest(".vip_agencies__section").find(".vip_agencies__name").html() + ',<br> в ближайшее время Вы получите ответ,<br> либо с Вами созвоняться менеджеры агентства <span class="vip_agencies__form__close"></span></div>');

    if (!emailValue) {
      emailInput.addClass('error');
      return false;
    }

    if (emailValue.indexOf('@') == -1) {
      emailInput.addClass('error');
      return false;
    }

    if (emailValue.indexOf('.') == -1) {
      emailInput.addClass('error');
      return false;
    }

    messageBlock.addClass('sending');
    okBlock.insertAfter(form);

    $.post(
      self.attr("href"),
      self.closest(".vip_agencies__form").serialize(),
      function (data) {
        if (data == 'ok') {
          console.log(data);
        }
        else {
          console.log(data);
        }
      },
      'html'
    );
    setTimeout(function () {
      okBlock.removeClass('js-hidden');
    }, 800);

    setTimeout(function () {
      messageBlock.removeClass('sending');
    }, 1000);

    $('.vip_agencies__form__close').click(function () {
      var self = $(this);

      $(this).parent()
        .parent()
        .find('.vip_agencies__form')
        .css({'z-index': -1, 'display': 'none', 'opacity': 0});

      $(this).parent().addClass('js-hidden');

      setTimeout(function () {

        self.parent()
          .parent()
          .find('.vip_agencies__info')
          .css({'z-index': 1, 'display': 'block'}).animate({opacity: 1}, 300);
      }, 500);

      return false;
    });


    return false;

  });

  // Убираем класс ошибки при наведении фокусе поля email
  $('body').on('focusin', '.vip_agencies__form__email', function (event) {
    $(this).removeClass('error');
  });

  // Подcписки филиалов в списке агентств

  $('body').on('click', '.show-agencies-sublist', function (event) {
    var self = $(this),
      sublist = self.siblings('ul');

    if (sublist.hasClass('js-hidden')) {
      sublist.removeClass('js-hidden');
      self.attr('rel', self.html());
      self.html('скрыть филиалы');
    } else {
      sublist.addClass('js-hidden');
      self.html(self.attr('rel'));
    }

    return false;
  });

  // Кнопка Наверх
  $('.map__button_up').click(function (e) {

    upPage();

    return false;
  });
  ;

});

// Функция для прокрутки страницы
var upPage = function () {
  var top = Math.max(document.body.scrollTop, document.documentElement.scrollTop);
  if (top > 0) {
    window.scrollBy(0, ((top + 100) / -10));
    t = setTimeout('upPage()', 20);
  } else clearTimeout(t);
  return false;
};

$(function () {
  $('#rr-city-field').autocomplete({
    minLength: 1,
    delay: 300,
    showAnim: 'fold',
    appendTo: '.rr-city-field-wrapper',
    source: function (request, response) {
      var city_id = jsParams['currentCityId'];
      //$('#city').addClass("ajax_wait");
      $.ajax({
        url: "/site/city", // <?= $this->createUrl('/site/city', ['withAgencies' => 1]) ?>",
        data: {'term': $('#rr-city-field').val()},
        dataType: "json",
        type: "GET",
        success: function (data) {
          response(data);
          //$('#city').removeClass("ajax_wait");
        },
        error: function (e) {
          //$('#city').removeClass("ajax_wait");
        }
      });
    },
    select: function (event, ui) {
      $.ajax({
        type: 'get',
        dataType: 'json',
        url: jsParams['agenciesUrlList'],
        data: {
          cityId: ui.item.id
          //page: page + 1 // передаём номер нужной страницы методом POST
        },
        beforeSend: function () {
          $('#loading').show();
          $('.map__infoblock').addClass('js-static');
        },
        success: function (data) {
          // увеличиваем номер текущей страницы и снимаем блокировку
          $('#loading').hide();
          //вставляем полученные записи после имеющихся в наш блок
          $('.agencies_blocks').parent().html(data.data);
          $('.m-region').html(ui.item.region_name);

          var count = ui.item.agenciesCount;
          var word = count > 0 ? ' ' + declension(count, ['агентство', 'агентства', 'агентств']) : 'агентств';
          $('.js-number_agencies').html(count);
          $('.js-number_agencies-work-genitive').html(word);
          $('.m-quantity').html(count ? (count + ' ' + word) : 'Нет агентств');
          $('.js-price-with-word').html(ui.item.price + ' ' + declension(ui.item.price, ['рубль', 'рубля', 'рублей']));
          changeLinksSubdomains(jsParams["currentCityDomain"], ui.item.city_domain);

          $('#Notice_city_id').val(ui.item.id);
          $('#city').val(ui.item.city_name + ' / ' + ui.item.region_name + ' / ' + count + word);

          $('.map__infoblock').removeClass('js-static');

          $('.footer').removeClass('js-hidden');

          jsParams['agenciesPagesCount'] = data.agenciesPagesCount;
          jsParams['currentCityName'] = data.city_name;
          jsParams['currentCityDomain'] = data.city_domain;
          jsParams['agenciesPage'] = 0;
          jsParams['currentCityId'] = ui.item.id;
          StoryBox.Init($('body'), {});

        }
      })

    }
  })

});


//склонение окончаний
function declension(num, expressions) {
  var result;
  count = num % 100;
  if (count >= 5 && count <= 20) {
    result = expressions['2'];
  } else {
    count = count % 10;
    if (count == 1) {
      result = expressions['0'];
    } else if (count >= 2 && count <= 4) {
      result = expressions['1'];
    } else {
      result = expressions['2'];
    }
  }
  return result;
}

// замена ссылок с субдоменами
var changeLinksSubdomains = function (old_domain, new_domain) {
  if (old_domain == '') return;
  $('#headerButtonsAll a, .r_steps a, .r_header a').each(function () {
    var link = $(this);
    link.prop('href', link.prop('href').replace(old_domain, new_domain));
  });
};

// Новый каталог
$(function() {
  
  $('.agencies_abc__button').click(function(event) {
    var sections = $('.agencies_abc__partial'),
        section = $(this).closest('.agencies_abc__partial');

        sections.removeClass('agencies_abc__partial--up');
        section.addClass('agencies_abc__partial--up');

    return false;
  });

  // Отдельная область видимости для блока поиска
  (function() {

    // Объекты, которые меняются при поиске
    var searchInput = $('.agencies_header_search'),
        searchIcon  = $('.agencies_header_search__icon'),
        searchResults = $('.agencies_results'),
        searchMessageEmpty = $('.content_inner--empty');

    // Функция, эмулирующая загрузку данных
    function ajaxGetResult(status) {
      searchIcon.addClass('m-wait');
      setTimeout(function() {

        if( status === true ) {
          searchResults.removeClass('js-hidden');
        }

        if( status === false ) {
          searchMessageEmpty.removeClass('js-hidden');
        }

        searchIcon.removeClass('m-wait').addClass('m-clear');
      }, 1000);

      return true;

    }

    // Прячем лишние переменные
    searchResults.addClass('js-hidden');
    searchMessageEmpty.addClass('js-hidden');

    // Презентация работы фильтра
    searchInput.keyup(function(event) {
      
      var self = $(this),
          currentText = self.val();

      if(!currentText.length) {
        // Прячем лишние переменные
        searchResults.addClass('js-hidden');
        searchMessageEmpty.addClass('js-hidden');        
      }


      // Для презентации при наборе любых трех символов - отобразить результаты
      if(currentText.length >= 3) {
      
        // Прячем лишние переменные
        searchResults.addClass('js-hidden');
        searchMessageEmpty.addClass('js-hidden');
        
        // Если набраны три нуля - то появится сообщение об отсутствии результатов
        if(currentText === '000') {
          ajaxGetResult(false);

          return false;
        }

        // В ином случае - эмулируем загрузку данных
        ajaxGetResult(true);

        return true;

      }

    });


    // Нажатие на кнопку очистки поля поиска
    searchIcon.click(function(event) {

      if (!$(this).hasClass('m-clear')) {
        return false;
      }

      searchInput.val('').keyup();
      $(this).removeClass('m-clear');

      return false;
    });


  })();

  // Кнопка наверх

  var buttonUp = $('.map__footer.m-bottom .map__button_up');

  $(window).scroll(function(event) {

    var pageY = window.pageYOffset || document.documentElement.scrollTop,
        pageBlock = $('.main');

    if(pageY > 300) {
      buttonUp.addClass('js-fixed')
      if (pageY >= (pageBlock.height() - $(window).height() + 40)) {
        buttonUp.removeClass('js-fixed')
      }
    } else {
      buttonUp.removeClass('js-fixed')
    }
  });

  $('.agencies_header__hidden_menu').click(function(event) {
    
    if($(this).hasClass('hover')) {
      $(this).removeClass('visible');

      $(this).removeClass('hover');
      return false;
    } else {
      $(this).addClass('visible');

      $(this).addClass('hover');
      return false;
    }


  });
});